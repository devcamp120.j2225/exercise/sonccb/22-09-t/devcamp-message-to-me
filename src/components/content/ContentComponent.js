import { Component } from "react";
import InputMessage from "./form/InputMessage";
import LikeImage from "./result/LikeImage";

class ContentComponent extends Component {
 render(){
  return(
   <>
    <InputMessage/>
    <LikeImage/>
   </>
  ); 
 }
}

export default ContentComponent;