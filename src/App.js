import 'bootstrap/dist/css/bootstrap.min.css'
import ContentComponent from './components/content/ContentComponent';
import TitleComponent from './components/tittle/TitleComponent';

function App() {
  return (
    <div className="container mt-3 text-center">
     <TitleComponent/>
     <ContentComponent/>
    </div>
  );
}

export default App;
